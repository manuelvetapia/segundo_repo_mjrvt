# segundo_repo_mjrvt
Nombre: Manuel de Jesus Ramiro Velazquez Tapia
Puesto: Arquitecto II IT Process & Devops
Actividades: 
-Capacitación a equipos en devtools
-Acompañamiento en migraciones y adocion de stack tecnologico
-Definición de procesos y normas para el uso de las tecnologias
-Estrategias de adopcion de Legacy a las nuevas plataformas
-Difución de conocimiento DevOps mediante platicas y talleres
Area: Arquitectura - IT governance, process and DevOps
